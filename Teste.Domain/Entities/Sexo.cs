﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Teste.Domain.Entities
{

    [DataContract]
    public class Sexo : BaseEntity
    {
       
        [DataMember(Name = "nome")]
        public string Nome { get; set; }
    }
}
