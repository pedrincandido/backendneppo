﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace TesteNeppo.App.viewModel
{
    [DataContract]
    public class EnderecoViewModel
    {
        [DataMember(Name = "id")]
        public int? Id { get; set; }

        [DataMember(Name = "rua")]
        public string Rua { get; set; }

        [DataMember(Name = "numero")]
        public int Numero { get; set; }

        [DataMember(Name = "bairro")]
        public string Bairro { get; set; }

        [DataMember(Name = "cep")]
        public string Cep { get; set; }

        [DataMember(Name = "person_id")]
        public int? PersonId { get; set; }
    }
}
