﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Teste.Domain.Entities;
using Teste.Domain.Repositories;
using TesteNeppo.App.viewModel;

namespace TesteNeppo.App.Services
{
    public class ContractEnderecoApp
    {
        private IRepository<Endereco> _rep;


        public ContractEnderecoApp(IRepository<Endereco> rep)
        {
            _rep = rep;
        }


        public virtual List<EnderecoViewModel> GetAll()
        {
            List<EnderecoViewModel> listEndereco = new List<EnderecoViewModel>();
            _rep.GetAll().ToList().ForEach(e =>
            {
                EnderecoViewModel endereco = new EnderecoViewModel
                {
                    Id = e.Id,
                    Rua = e.Rua,
                    Bairro = e.Bairro,
                    Cep = e.Cep,
                    Numero = e.Numero,
                    PersonId = e.PersonId
                };

                listEndereco.Add(endereco);
            });

            return listEndereco;
        }

   
        public virtual EnderecoViewModel GetById(int id)
        {
            try
            {
               
                var entity = _rep.Get(id);

                EnderecoViewModel endereco = new EnderecoViewModel
                {
                    Id = entity.Id,
                    Rua = entity.Rua,
                    Bairro = entity.Bairro,
                    Cep = entity.Cep,
                    Numero = entity.Numero,
                    PersonId = entity.PersonId
                };

                return endereco;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public virtual Endereco SaveEndereco(EnderecoViewModel modelView)
        {
            try
            {

                Endereco entity = new Endereco
                {
                    Rua     = modelView.Rua,
                    Bairro  = modelView.Bairro,
                    Numero  = modelView.Numero,
                    Cep     = modelView.Cep,
                    PersonId = modelView.PersonId
                };
                _rep.Insert(entity);

                return entity;
            }
            catch
            {
                throw new ValidationException();
            }

        }


        public virtual Endereco EditPessoa(EnderecoViewModel view)
        {
            try
            {
             
                Endereco endereco = _rep.Get(view.Id.Value);

                if (endereco != null)
                {
                    view.Rua        = endereco.Rua;
                    view.Bairro     = endereco.Bairro;
                    view.Cep        = endereco.Cep;
                    view.Numero     = endereco.Numero;
                    view.PersonId   = endereco.PersonId;
                    _rep.Update(endereco);
                }

                return endereco;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteEndereco(int id)
        {
            Endereco endereco = _rep.Get(id);
            if (endereco != null)
            {
                _rep.Delete(endereco);
            }
        }
    }
}
