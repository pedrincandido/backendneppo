﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Teste.Domain.Entities;
using Teste.Domain.Repositories;
using TesteNeppo.App.viewModel;

namespace TesteNeppo.App.Services
{
    public class ContractAppPessoa
    {
        private IRepository<Pessoa> _rep;


        public ContractAppPessoa(IRepository<Pessoa> rep)
        {
            _rep = rep;
        }


        public virtual PessoaViewModel GetById(int id)
        {
            try
            {
                var entity = _rep.Get(id);

                PessoaViewModel person = new PessoaViewModel
                {
                    Id = entity.Id,
                    Nome = entity.Nome,
                    Cpf = entity.Cpf,
                    DataNascimento = entity.DataNascimento,
                    SexoId = entity.SexoId

                };
                return person;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual Pessoa GetByNome(string nome)
        {
            return  _rep.GetAll().Where(x => x.Nome == nome).FirstOrDefault();
        }

        public virtual Pessoa SavePessoa(PessoaViewModel modelView)
        {
            try
            {

                Pessoa entity = new Pessoa
                {
                    Nome = modelView.Nome,
                    Cpf = modelView.Cpf,
                    DataNascimento = modelView.DataNascimento,
                    SexoId = modelView.SexoId
                };
                _rep.Insert(entity);

                return entity;
            }
            catch
            {
                throw new ValidationException();
            }

        }


        public virtual Pessoa EditPessoa(PessoaViewModel view)
        {
            try
            {
                //PessoaViewModel view = new PessoaViewModel();
                Pessoa pessoa = _rep.Get(view.Id.Value);

                if (pessoa != null)
                {
                    pessoa.Nome = view.Nome;
                    pessoa.SexoId = view.SexoId;
                    pessoa.DataNascimento = view.DataNascimento;
                    _rep.Update(pessoa);
                }

                return pessoa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeletePessoa(int id)
        {
            Pessoa pessoa = _rep.Get(id);
            if (pessoa != null)
            {
                _rep.Delete(pessoa);
            }
        }


        public virtual List<PessoaViewModel> GetAll()
        {
            //IEnumerable<Pessoa> pessoa = _rep.GetAll();
            List<PessoaViewModel> listPessoa = new List<PessoaViewModel>();
            _rep.GetAll().ToList().ForEach(p =>
            {
                PessoaViewModel person = new PessoaViewModel
                {
                    Id = p.Id,
                    Nome = p.Nome,
                    Cpf = p.Cpf,
                    DataNascimento = p.DataNascimento,
                    SexoId = p.SexoId
                };

                listPessoa.Add(person);
            });

            return listPessoa;
              
        }
    }
}
