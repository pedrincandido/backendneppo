﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Teste.Domain.Entities;
using TesteNeppo.App.Services;
using TesteNeppo.App.viewModel;

namespace TesteNovo.Controllers
{
    
    public class EnderecoController : Controller
    {



        [HttpGet]
        [Produces(typeof(Endereco))]
        [Route("/endereco/{id}")]
        public IActionResult Get(int id, [FromServices] ContractEnderecoApp contractEndereco)
        {
            try
            {
                return Ok(contractEndereco.GetById(id));
            }
            catch
            {
                return NotFound(id);
            }
        }


        [HttpGet]
        [Produces(typeof(List<Endereco>))]
        [Route("/endereco")]
        public IActionResult Get([FromServices] ContractEnderecoApp contractEndereco)
        {
            try
            {
                return Ok(contractEndereco.GetAll());
            }
            catch
            {
                return NotFound();
            }

        }




        [HttpPost]
        [Route("endereco")]
        public IActionResult Post([FromBody]EnderecoViewModel body, [FromServices] ContractEnderecoApp contractEndereco)
        {
            try
            {
                contractEndereco.SaveEndereco(body);
                return Ok(HttpStatusCode.OK);
            }
            catch
            {
                return BadRequest();
            }


        }
      
        [HttpPut]
        [Route("endereco")]
        public IActionResult Put([FromBody]EnderecoViewModel body, [FromServices] ContractEnderecoApp contractEndereco)
        {
            try
            {
                contractEndereco.EditPessoa(body);
                return Ok(HttpStatusCode.OK);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Route("endereco/{id}")]
        public IActionResult Delete(int id, [FromServices] ContractEnderecoApp contractEndereco)
        {
            if (id > 0)
                contractEndereco.DeleteEndereco(id);

            else
                BadRequest();

            return Ok(HttpStatusCode.OK);
        }
    }
}