﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Teste.Domain.Entities;
using TesteNeppo.App.Services;
using TesteNeppo.App.viewModel;

namespace TesteNovo.Controllers
{
  

    public class PessoaController : Controller
    {



        [HttpGet]
        [Produces(typeof(Pessoa))]
        [Route("/pessoa/{id}")]
        public IActionResult Get(int id, [FromServices] ContractAppPessoa contractPersonApp)
        {
            try
            {
                return Ok(contractPersonApp.GetById(id));
            }
            catch
            {
                return NotFound(id);
            }
        }


        [HttpGet]
        [Produces(typeof(List<Pessoa>))]
        [Route("/pessoa")]
        public IActionResult Get([FromServices] ContractAppPessoa contractPersonApp)
        {
            try
            {
                return Ok(contractPersonApp.GetAll());
            }
            catch
            {
                return NotFound();
            }

        }

 

        [HttpPost]
        [Route("pessoa")]
        public IActionResult Post([FromBody]PessoaViewModel body, [FromServices] ContractAppPessoa contractPersonApp)
        {
            try
            {
                contractPersonApp.SavePessoa(body);
                return Ok(HttpStatusCode.OK);
            }
            catch
            {
                return BadRequest();
            }
        }
 
  

        [HttpPut]
        [Route("pessoa")]
        public IActionResult Put([FromBody]PessoaViewModel body, [FromServices] ContractAppPessoa contractPersonApp)
        {
            try
            {
                contractPersonApp.EditPessoa(body);
                return Ok(HttpStatusCode.OK);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Route("pessoa/{id}")]
        public IActionResult Delete(int id, [FromServices] ContractAppPessoa contractPersonApp)
        {
            if (id > 0)
                contractPersonApp.DeletePessoa(id);

            else
                BadRequest();

            return Ok(HttpStatusCode.OK);
        }
    }
}