﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Teste.Domain.Entities;

namespace Teste.Impl.Configuration
{
    public class EnderecoConfig : IEntityTypeConfiguration<Endereco>
    {
        public void Configure(EntityTypeBuilder<Endereco> entity)
        {
            entity.ToTable("ENDERECO");


            entity.HasKey(e => e.Id);
            entity.Property(e => e.Id).HasColumnName("ID").UseSqlServerIdentityColumn().IsRequired();
            entity.Property(e => e.Rua).HasColumnName("RUA").HasMaxLength(100).IsRequired();
            entity.Property(e => e.Cep).HasColumnName("CEP").HasMaxLength(16).IsRequired();
            entity.Property(e => e.Bairro).HasColumnName("BAIRRO").HasMaxLength(30).IsRequired();
            entity.Property(e => e.Numero).HasColumnName("NUMERO").IsRequired();
            entity.Property(e => e.PersonId).HasColumnName("PESSOA_ID").IsRequired(false);

        }

     
    }
}
